﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityDotNET
{
    public class City
    {
        private string _name;
        private string _postCode;
        private string _province;

        public City()
        {
            this._name = "";
            this._postCode = "";
            this._province = "";
        }

        public City(City city)
        {
            this._name = city.Name;
            this._postCode = city.PostCode;
            this._province = city.Province;
        }

        public City(string name, string postCode, string province) : this()
        {
            this.Name = name;
            this.PostCode = postCode;
            this.Province = province;
        }

        public string Name
        {
            get => this._name;
            set => this._name = value;
        }

        public string PostCode
        {
            get => this._postCode;
            set => this._postCode = value;
        }

        public string Province
        {
            get => this._province;
            set => this._province = value;
        }
    }
}
