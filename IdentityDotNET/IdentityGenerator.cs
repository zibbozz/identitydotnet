﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityDotNET
{
    public class IdentityGenerator
    {
        // 0 = all, 1 = male, 2 = female
        private byte _gender;
        private short _minYear;
        private short _maxYear;

        public IdentityGenerator()
        {
            this._gender = 0;
            this._minYear = 1900;
            this._maxYear = 2020;
        }

        public IdentityGenerator(byte gender) : this()
        {
            this.Gender = gender;
        }

        public byte Gender
        {
            get => this._gender;
            set => this._gender = value >= 0 && value <= 2 ? value : throw new ArgumentException("Gender can only be 0 (all), 1 (male) or 2 (female)");
        }

        public short MinYear
        {
            get => this._minYear;
            set => this._minYear = value >= 0 ? value : throw new ArgumentException("The given year can not be under 0");
        }

        public short MaxYear
        {
            get => this._maxYear;
            set => this._maxYear = value >= 0 ? value : throw new ArgumentException("The given year can not be under 0");
        }

        public Identity Generate()
        {
            IdentityData data = new IdentityData();
            Identity identity = new Identity();
            Random rnd = new Random();
            List<string> name = new List<string>();
            switch (this.Gender)
            {
                case 0:
                    name.AddRange(data.DeFirstNamesMale);
                    name.AddRange(data.DeFirstNamesFemale);
                    break;
                case 1:
                    name.AddRange(data.DeFirstNamesMale);
                    break;
                case 2:
                    name.AddRange(data.DeFirstNamesFemale);
                    break;
            }

            identity.FirstName = name[rnd.Next(0, name.Count)];
            identity.LastName = data.DeLastNames[rnd.Next(0, data.DeLastNames.Count)];
            identity.YearOfBirth = (short)rnd.Next((int)this._minYear, (int)this._maxYear);
            identity.MonthOfBirth = (byte)rnd.Next(1, 13);
            identity.DayOfBirth = (byte)rnd.Next(1, 29);
            City tempCity = data.DeCities[rnd.Next(0, data.DeCities.Count)];
            identity.City = tempCity.Name;
            identity.PostCode = tempCity.PostCode;
            identity.Street = data.DeStreets[rnd.Next(0, data.DeStreets.Count)];
            identity.HouseNumberDecimal = (byte) rnd.Next(1, 100);
            identity.HouseNumberExtra = rnd.Next(0, 100) > 90 ? (char)rnd.Next(97, 105) : ' ';
            identity.Job = data.DeJobs[rnd.Next(0, data.DeJobs.Count)];
            identity.CarType = data.Cars[rnd.Next(0, data.Cars.Count)];
            return identity;
        }
    }
}
