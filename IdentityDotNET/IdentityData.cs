﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IdentityDotNET
{
    public class IdentityData
    {
        private List<string> _deFirstNamesMale;
        private List<string> _deFirstNamesFemale;
        private List<string> _deLastNames;
        private List<City> _deCities;
        private List<string> _deStreets;
        private List<string> _deJobs;
        private List<string> _cars;

        public IdentityData()
        {
            this._deFirstNamesMale = new List<string>();
            this._deFirstNamesFemale = new List<string>();
            this._deLastNames = new List<string>();
            this._deCities = new List<City>();
            this._deStreets = new List<string>();
            this._deJobs = new List<string>();
            this._cars = new List<string>();

            string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            StreamReader sr = new StreamReader(appPath + @"\Data\de_first_names_male.csv");
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                this._deFirstNamesMale.Add(line);
            }
            sr.Close();

            sr = new StreamReader(appPath + @"\Data\de_first_names_female.csv");
            while ((line = sr.ReadLine()) != null)
            {
                this._deFirstNamesFemale.Add(line);
            }
            sr.Close();

            sr = new StreamReader(appPath + @"\Data\de_last_names.csv");
            while ((line = sr.ReadLine()) != null)
            {
                this._deLastNames.Add(line);
            }
            sr.Close();

            sr = new StreamReader(appPath + @"\Data\de_cities.csv");
            while ((line = sr.ReadLine()) != null)
            {
                string[] temp = line.Split(';');
                this._deCities.Add(new City(temp[0], temp[1], temp[2]));
            }
            sr.Close();

            sr = new StreamReader(appPath + @"\Data\de_streets.csv");
            while ((line = sr.ReadLine()) != null)
            {
                this._deStreets.Add(line);
            }
            sr.Close();

            sr = new StreamReader(appPath + @"\Data\de_jobs.csv");
            while ((line = sr.ReadLine()) != null)
            {
                this._deJobs.Add(line);
            }
            sr.Close();

            sr = new StreamReader(appPath + @"\Data\cars.csv");
            while ((line = sr.ReadLine()) != null)
            {
                this._cars.Add(line);
            }
            sr.Close();
        }

        public List<string> DeFirstNamesMale => this._deFirstNamesMale;

        public List<string> DeFirstNamesFemale => this._deFirstNamesFemale;

        public List<string> DeLastNames => this._deLastNames;
        public List<City> DeCities => this._deCities;

        public List<string> DeStreets => this._deStreets;

        public List<string> DeJobs => this._deJobs;

        public List<string> Cars => this._cars;
    }
}