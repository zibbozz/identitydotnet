﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IdentityDotNET
{
    public class Identity
    {
        private string _firstName;
        private string _lastName;
        private short _yearOfBirth;
        private byte _monthOfBirth;
        private byte _dayOfBirth;
        private string _job;
        private string _company;
        private string _street;
        private byte _houseNumber;
        private char _houseNumberExtra;
        private string _city;
        private string _postCode;
        private string _favoriteColor;
        private string _carType;

        private string[] _months;

        public Identity()
        {
            this._firstName = "";
            this._lastName = "";
            this._yearOfBirth = 1900;
            this._monthOfBirth = 1;
            this._dayOfBirth = 1;
            this._job = "";
            this._company = "";
            this._street = "";
            this._houseNumber = 1;
            this._houseNumberExtra = ' ';
            this._city = "";
            this._postCode = "";
            this._favoriteColor = "";
            this._carType = "";
            this._months = new string[]
            {
                "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"
            };
        }

        public Identity(Identity identity)
        {
            this._firstName = identity.FirstName;
            this._lastName = identity.LastName;
            this._yearOfBirth = identity.YearOfBirth;
            this._monthOfBirth = identity.MonthOfBirth;
            this._dayOfBirth = identity.DayOfBirth;
            this._job = identity.Job;
            this._company = identity.Company;
            this._street = identity.Street;
            this._houseNumber = identity.HouseNumberDecimal;
            this._houseNumberExtra = identity.HouseNumberExtra;
            this._city = identity.City;
            this._postCode = identity.PostCode;
            this._favoriteColor = identity.FavoriteColor;
            this._carType = identity.CarType;
        }

        public Identity(string firstName, string lastName) : this()
        {
            this.FirstName = firstName;
            this.LastName = LastName;
        }

        public Identity(string firstName, string lastName, short yearOfBirth, byte monthOfBirth, byte dayOfBirth) : this(firstName, lastName)
        {
            this.YearOfBirth = yearOfBirth;
            this.MonthOfBirth = monthOfBirth;
            this.DayOfBirth = dayOfBirth;
        }

        public string FirstName
        {
            get => this._firstName;
            set
            {
                bool valid = Regex.IsMatch(value, @"\A^[a-zA-ZäöüÄÖÜ][A-Za-zäöüÄÖÜ\-ß]+\z", RegexOptions.Multiline);
                if (valid)
                    this._firstName = value;
                else
                    throw new ArgumentException("The given name is not valid");
            }
        }

        public string LastName
        {
            get => this._lastName;
            set
            {
                bool valid = Regex.IsMatch(value, @"\A^[a-zA-ZäöüÄÖÜ][A-Za-zäöüÄÖÜ\-ß]+\z", RegexOptions.Multiline);
                if (valid)
                    this._lastName = value;
                else
                    throw new ArgumentException("The given name is not valid");
            }
        }

        public short YearOfBirth
        {
            get => this._yearOfBirth;
            set => this._yearOfBirth = value >= 0 ? value : throw new ArgumentException("The given year can not be under 0");
        }

        public byte MonthOfBirth
        {
            get => this._monthOfBirth;
            set => this._monthOfBirth = value >= 1 && value <= 12 ? value : throw new ArgumentException("The given month must be between 1 and 12");
        }

        public byte DayOfBirth
        {
            get => this._dayOfBirth;
            set => this._dayOfBirth = value >= 1 && value <= 31 ? value : throw new ArgumentException("The given day must be between 1 and 31");
        }

        public string Birthday => this.DayOfBirth + ". " + this._months[this.MonthOfBirth - 1] + " " + this.YearOfBirth;

        public string Job
        {
            get => this._job;
            set => this._job = value;
        }

        public string Company
        {
            get => this._company;
            set => this._company = value;
        }

        public string Street
        {
            get => this._street;
            set => this._street = value;
        }

        public byte HouseNumberDecimal
        {
            get => this._houseNumber;
            set => this._houseNumber = value >= 1 ? value : throw new ArgumentException("The given house number must be at least 1");
        }

        public char HouseNumberExtra
        {
            get => this._houseNumberExtra;
            set => this._houseNumberExtra = value;
        }

        public string HouseNumber => this.HouseNumberDecimal.ToString() + this.HouseNumberExtra.ToString();

        public string City
        {
            get => this._city;
            set => this._city = value;
        }

        public string PostCode
        {
            get => this._postCode;
            set => this._postCode = value;
        }

        public string FavoriteColor
        {
            get => this._favoriteColor;
            set => this._favoriteColor = value;
        }

        public string CarType
        {
            get => this._carType;
            set => this._carType = value;
        }
    }
}
