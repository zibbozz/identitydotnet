# Changelog

During the development of this project all changes will be written down in this changelog. The version numbers are following the principle of the [semantic versioning](https://semver.org)

## [0.1.0] - 2020-08-29
### Added
- Basic library to read data from csv files and generate an identity out of it
- Basic GUI to use the library for simple identity generation
- Most used german first and last names
- All german cities including post code
- All street names in germany
- Common jobs in germany
- Most driven car types
