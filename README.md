# Identity.NET

This project is used to generate your own random identity. If you have to register on a website where you don't want to use your real name and data, you can just quickly generate a random identity and use this data. But be aware that the genererated data does not always makes sense. For example an identity that is underage still gets a job and a car type assigned.

## Future of Identity.NET
Here is a small list what my goals are for this project
- More Languages supported in the GUI
- Data from other countries
- More in-depth generator to create identites that make more sense

## Usage of the Identity.NET library
The following examples are a quick help to understand how to use the library.

### Generate an identity
```c#
IdentityGenerator generator = new IdentityGenerator();
Identity identity = generator.Generate();
```