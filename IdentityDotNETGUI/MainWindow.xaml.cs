﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IdentityDotNET;

namespace IdentityDotNETGUI
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>

    // Farben
    // Primär: #37474F (Blaugrau 800)
    // Hell: #62727B
    // Dunkel: #102027
    // Sekundär: #64DD17 (Hellgrün A700)
    // Hell: #9CFF57
    // Dunkel: #1FAA00
    // Hintergrund: #DDDDE2
    
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void GenerateButton_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IdentityGenerator generator = new IdentityGenerator((byte)genderComboBox.SelectedIndex);
            generator.MaxYear = 2000;
            Identity identity = generator.Generate();
            nameTextBlock.Text = identity.FirstName + " " + identity.LastName;
            addressTextBlock.Text = identity.Street + " " + identity.HouseNumber;
            cityTextBlock.Text = identity.PostCode + " " + identity.City;
            birthdayTextBlock.Text = identity.Birthday;
            jobTextBlock.Text = identity.Job;
            carTextBlock.Text = identity.CarType;
        }
    }
}
